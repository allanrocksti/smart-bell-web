import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { IdeiaComponent } from './ideia/ideia.component';
import { CanvasComponent } from './canvas/canvas.component';
import { StudentsComponent } from './students/students.component';
import { DiagramsComponent } from './diagrams/diagrams.component';
import { MockupComponent } from './mockup/mockup.component';

@NgModule({
  declarations: [
    AppComponent,
    IdeiaComponent,
    CanvasComponent,
    StudentsComponent,
    DiagramsComponent,
    MockupComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
