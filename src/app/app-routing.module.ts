import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { IdeiaComponent } from './ideia/ideia.component';
import { CanvasComponent } from './canvas/canvas.component';
import { StudentsComponent } from './students/students.component';
import { DiagramsComponent } from './diagrams/diagrams.component';
import { MockupComponent } from './mockup/mockup.component';

const routes: Routes = [
  { path: 'ideia', component: IdeiaComponent },
  { path: 'canvas', component: CanvasComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'diagrams', component: DiagramsComponent },
  { path: 'mockup', component: MockupComponent },
  { path: '', redirectTo: '/ideia', pathMatch: 'full'},
  { path: '**', redirectTo: '/ideia', pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
