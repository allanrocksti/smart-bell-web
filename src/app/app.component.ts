import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  active: string;

  constructor(
    private _router: Router
  ) { }

  isActiveLink(link: string) {
    if (link === '/ideia') {
      this.active = 'active';
    } else {
      this.active = '';
    }
    return this._router.isActive(link, false);
  }

  navigateToIdeia() {
    this._router.navigate(['/ideia']);
  }

  navigateToCanvas() {
    this._router.navigate(['/canvas']);
  }

  navigateToDiagrams() {
    this._router.navigate(['/diagrams']);
  }

  navigateToMockup() {
    this._router.navigate(['/mockup']);
  }

  navigateToStudents() {
    this._router.navigate(['/students']);
  }

}
